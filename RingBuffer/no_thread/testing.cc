// Ring Buffer Testing

#include <cassert>
#include <iostream>

#include "ring_buffer.hh"

void fill(bounded_queue<int>& queue) {
  int i = 0;
  for(; !queue.is_full(); ++i) {
    assert(queue.push(i));
  }
  assert((unsigned)i == queue.getsize());
  assert(i == 256);
}


void clear(bounded_queue<int>& queue) {
  unsigned i = 0;
  for (; !queue.is_empty(); ++i) {
    int x;
    assert(queue.top(x) && x == (int)i);
    x = 0;
    assert(queue.pop(x) && x == (int)i);
  }
  assert(queue.getsize() == 0);
}

void both(bounded_queue<int>& queue) {
  unsigned i=0;
  int j=0;
  assert(queue.push(i));
  while (!queue.is_full()) {
    int x;
    i += 1;
    assert(queue.push(i));
    i += 1;
    assert(queue.pop(x));
    assert(queue.push(i));
    assert(x == j);
    j = x+1;
  }
  queue.iter([=](int& i){ std::cout << "| " << i << " "; });
  std::cout << "|" << std::endl;
  // clear(queue);
}

int main() {
  bounded_queue<int>        queue(256);
  fill(queue);
  clear(queue);
  both(queue);
}
