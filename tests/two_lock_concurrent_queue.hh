/*
 * Copyright (c) 2013, Marwan Burelle
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// Two locks concurrent queue
// based on Maged M. Michael and Michael L. Scott article (PODC96)

#include <mutex>

#ifndef LOCKFREEEXPERIMENT_TWO_LOCK_CONCURRENT_QUEUE_HH_
#define LOCKFREEEXPERIMENT_TWO_LOCK_CONCURRENT_QUEUE_HH_

template<typename T>
class two_lock_queue {
public:
  two_lock_queue() {
    node               *n = new node();
    Head = Tail = n;
  }

  void push(T value) noexcept {
    node               *n = new node(value);
    { std::lock_guard<std::mutex>       lock(T_lock);
      Tail->next = n;
      Tail = n;
    }
  }

  bool pop(T& rvalue) noexcept {
    node               *old;
    { std::lock_guard<std::mutex>       lock(H_lock);
      if (Head->next == 0)
        return false;
      old = Head;
      node             *n = Head->next;
      rvalue = n->value;
      Head = n;
    }
    delete old;
    return true;
  }

private:
  struct node {
    node               *next;
    T                   value;

    node()      : next(0)            {}
    node(T v)   : next(0), value (v) {}
  };

  node                 *Head, *Tail;
  std::mutex            H_lock, T_lock;

};

#endif
