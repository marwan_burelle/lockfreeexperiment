/*
 * Copyright (c) 2013, Marwan Burelle
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// giant-locked queue
// naive concurrent queue

#include <mutex>

#ifndef LOCKFREEEXPERIMENT_LOCKED_QUEUE_HH_
#define LOCKFREEEXPERIMENT_LOCKED_QUEUE_HH_

template<typename T>
class locked_queue {
public:
  locked_queue() : q(0) {}

  void push(T x) {
    node                               *n = new node(x);
    { std::lock_guard<std::mutex>       lock(_lock);
      if (q) {
        n->next = q->next;
        q->next = n;
      } else n->next = n;
      q = n;
    }
  }

  bool pop(T& x) {
    node                               *n;
    { std::lock_guard<std::mutex>       lock(_lock);
      if (q == NULL) return false;
      n = q->next;
      x = n->value;
      if (n != q) {
        q->next = n->next;
      } else q = 0;
    }
    delete n;
    return true;
  }

private:
  struct node {
    T           value;
    node       *next;
    node()              : next(0) {}
    node(T x)           : value(x), next(0) {}
    node(T x, node *n)  : value(x), next(n) {}
  };

  node         *q;
  std::mutex    _lock;

};

#endif
