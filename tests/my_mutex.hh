// Simple spinlock for test

#include <atomic>

#ifndef LOCKFREEEXPERIMENT_MY_MUTEX_HH_
#define LOCKFREEEXPERIMENT_MY_MUTEX_HH_

class marwanlock {
public:
  marwanlock() : flag(ATOMIC_FLAG_INIT) {}

  void lock() noexcept {
    while (flag.test_and_set()) {
      //std::this_thread::yield();
    }
  }

  void unlock() noexcept { flag.clear(); }

private:
  std::atomic_flag      flag;
};

#endif
